import pandas as pd
from keras.preprocessing.text import Tokenizer
import tensorflow
from keras.utils import pad_sequences
from sklearn.model_selection import train_test_split
import warnings
warnings.filterwarnings('ignore')

import pickle


final_df = pd.read_csv('main_data.csv')

final_df['incorrect_word_count'] = final_df['input'].astype('str').apply(lambda x:len(x.split()))
final_df = final_df[final_df['incorrect_word_count'] <= 12]

final_df = final_df[['input','output']]

train,test = train_test_split(final_df, test_size=0.1,random_state=15)
train,validation = train_test_split(train, test_size=0.1)

train['dec_correct_inp'] = ' ' + train['output'].astype(str)
train['dec_correct_out'] = train['output'].astype(str) + ' '

validation['dec_correct_inp'] = ' ' + validation['output'].astype(str)
validation['dec_correct_out'] = validation['output'].astype(str) + ' '

test['dec_correct_inp'] = ' ' + test['output'].astype(str)
test['dec_correct_out'] = test['output'].astype(str) + ' '


train.to_csv('data/train.csv')
validation.to_csv('data/validation.csv')
test.to_csv('data/test.csv')


tokenizer_enc = Tokenizer(filters='#$%&()*+/=?@[\]^_`{|}~\t\n',lower=False)
tokenizer_enc.fit_on_texts(train['input'].astype(str).values)

tokenizer_dec = Tokenizer(filters='#$%&()*+/=?@[\]^_`{|}~\t\n',lower=False)
tokenizer_dec.fit_on_texts(train['dec_correct_inp'].astype(str).values)

vocab_size_enc=len(tokenizer_enc.word_index.keys())
print('Encoder words vocab size:',vocab_size_enc)

vocab_size_dec=len(tokenizer_dec.word_index.keys())
print('Decoder words vocab size::',vocab_size_dec)

tokenizer_dec.word_index[''] = max(tokenizer_dec.word_index.values()) + 1

with open('data/tokenizer_enc.pkl', 'wb') as f:
  pickle.dump(tokenizer_enc, f)

with open('data/tokenizer_dec.pkl', 'wb') as f:
  pickle.dump(tokenizer_dec, f)


tokenizer_enc = Tokenizer(filters='#$%&()*+/=?@[\]^_`{|}~\t\n',lower=False)
tokenizer_enc.fit_on_texts(train['input'].astype(str).values)
tokenizer_dec = Tokenizer(filters='#$%&()*+/=?@[\]^_`{|}~\t\n',lower=False)
tokenizer_dec.fit_on_texts(train['dec_correct_inp'].astype(str).values)

vocab_size_enc=len(tokenizer_enc.word_index.keys())
print('Encoder words vocab size:',vocab_size_enc)
vocab_size_dec=len(tokenizer_dec.word_index.keys())
print('Decoder words vocab size::',vocab_size_dec)


#text2seq
train_enc_inp = tokenizer_enc.texts_to_sequences(train['input'].astype(str).values)
validation_enc_inp = tokenizer_enc.texts_to_sequences(validation['input'].astype(str).values)
test_enc_inp = tokenizer_enc.texts_to_sequences(test['input'].astype(str).values)

#padding
max_seq_len = 12
train_enc_inp = pad_sequences(train_enc_inp, maxlen=max_seq_len, padding='post', truncating='post')
validation_enc_inp = pad_sequences(validation_enc_inp, maxlen=max_seq_len, padding='post', truncating='post')

#text2seq
train_dec_inp = tokenizer_dec.texts_to_sequences(train['dec_correct_inp'].astype(str).values)
validation_dec_inp = tokenizer_dec.texts_to_sequences(validation['dec_correct_inp'].astype(str).values)

#padding
train_dec_inp = pad_sequences(train_dec_inp, maxlen=max_seq_len+1, padding='post', truncating='post')
validation_dec_inp = pad_sequences(validation_dec_inp, maxlen=max_seq_len+1, padding='post', truncating='post')

#text2seq
train_dec_out = tokenizer_dec.texts_to_sequences(train['dec_correct_out'].astype(str).values)
validation_dec_out = tokenizer_dec.texts_to_sequences(validation['dec_correct_out'].astype(str).values)

#padding
train_dec_out = pad_sequences(train_dec_out, maxlen=max_seq_len+1, padding='post', truncating='post')
validation_dec_out = pad_sequences(validation_dec_out, maxlen=max_seq_len+1, padding='post', truncating='post')


#text2seq
train_enc_inp = tokenizer_enc.texts_to_sequences(train['input'].astype(str).values)
validation_enc_inp = tokenizer_enc.texts_to_sequences(validation['input'].astype(str).values)
test_enc_inp = tokenizer_enc.texts_to_sequences(test['input'].astype(str).values)

#padding
max_seq_len = 12
train_enc_inp = pad_sequences(train_enc_inp, maxlen=max_seq_len, padding='post', truncating='post')
validation_enc_inp = pad_sequences(validation_enc_inp, maxlen=max_seq_len, padding='post', truncating='post')
test_enc_inp = pad_sequences(test_enc_inp, maxlen=max_seq_len, padding='post', truncating='post')


#text2seq
train_dec_inp = tokenizer_dec.texts_to_sequences(train['dec_correct_inp'].astype(str).values)
validation_dec_inp = tokenizer_dec.texts_to_sequences(validation['dec_correct_inp'].astype(str).values)
test_dec_inp = tokenizer_dec.texts_to_sequences(test['dec_correct_inp'].astype(str).values)

#padding
train_dec_inp = pad_sequences(train_dec_inp, maxlen=max_seq_len+1, padding='post', truncating='post')
validation_dec_inp = pad_sequences(validation_dec_inp, maxlen=max_seq_len+1, padding='post', truncating='post')
test_dec_inp = pad_sequences(test_dec_inp, maxlen=max_seq_len+1, padding='post', truncating='post')

#text2seq
train_dec_out = tokenizer_dec.texts_to_sequences(train['dec_correct_out'].astype(str).values)
validation_dec_out = tokenizer_dec.texts_to_sequences(validation['dec_correct_out'].astype(str).values)
test_dec_out = tokenizer_dec.texts_to_sequences(test['dec_correct_out'].astype(str).values)

#padding
train_dec_out = pad_sequences(train_dec_out, maxlen=max_seq_len+1, padding='post', truncating='post')
validation_dec_out = pad_sequences(validation_dec_out, maxlen=max_seq_len+1, padding='post', truncating='post')
test_dec_out = pad_sequences(test_dec_out, maxlen=max_seq_len+1, padding='post', truncating='post')

with open('data/train_enc_inp.pkl', 'wb') as f:
  pickle.dump(train_enc_inp, f)

with open('data/val_enc_inp.pkl', 'wb') as f:
  pickle.dump(validation_enc_inp, f)

with open('data/test_enc_inp.pkl', 'wb') as f:
  pickle.dump(test_enc_inp, f)

with open('data/train_dec_inp.pkl', 'wb') as f:
  pickle.dump(train_dec_inp, f)

with open('data/val_dec_inp.pkl', 'wb') as f:
  pickle.dump(validation_dec_inp, f)

with open('data/test_dec_inp.pkl', 'wb') as f:
  pickle.dump(test_dec_inp, f)

with open('data/train_dec_out.pkl', 'wb') as f:
  pickle.dump(train_dec_out, f)

with open('data/val_dec_out.pkl', 'wb') as f:
  pickle.dump(validation_dec_out, f)

with open('data/test_dec_out.pkl', 'wb') as f:
  pickle.dump(test_dec_out, f)