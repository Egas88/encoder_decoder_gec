import warnings
warnings.filterwarnings('ignore')
import zipfile
import pandas as pd
import numpy as np
import re
import matplotlib.pyplot as plt
import tensorflow as tf
from keras.layers import Embedding, LSTM, Dense,RNN
from keras.models import Model
from keras.preprocessing.text import Tokenizer
from keras.utils import pad_sequences
from tqdm import tqdm
from sklearn.model_selection import train_test_split
import seaborn as sns
import os
from datetime import datetime
import pytz
from keras.callbacks import LearningRateScheduler,EarlyStopping,ModelCheckpoint,TensorBoard,ReduceLROnPlateau
from keras.optimizers import Adam,Nadam
import nltk.translate.bleu_score as bleu
import random
from tqdm import tqdm
import shutil
import io
from nltk.translate.gleu_score import sentence_gleu
import pickle
from keras.layers import Input, Softmax

tokenizer_enc = pickle.load(open(r"data/tokenizer_enc.pkl", "rb"))
tokenizer_dec = pickle.load(open(r"data/tokenizer_dec.pkl", "rb"))

train_enc_inp = pickle.load(open(r"data/train_enc_inp.pkl", "rb"))
val_enc_inp = pickle.load(open(r"data/val_enc_inp.pkl", "rb"))
test_enc_inp = pickle.load(open(r"data/test_enc_inp.pkl", "rb"))

train_dec_inp = pickle.load(open(r"data/train_dec_inp.pkl", "rb"))
val_dec_inp = pickle.load(open(r"data/val_dec_inp.pkl", "rb"))
test_dec_inp = pickle.load(open(r"data/test_dec_inp.pkl", "rb"))

train_dec_out = pickle.load(open(r"data/train_dec_out.pkl", "rb"))
val_dec_out = pickle.load(open(r"data/val_dec_out.pkl", "rb"))
test_dec_out = pickle.load(open(r"data/test_dec_out.pkl", "rb"))


class Encoder(tf.keras.layers.Layer):
    '''
    Encoder model -- That takes a input sequence and returns encoder-outputs,encoder_final_state_h,encoder_final_state_c
    '''

    def __init__(self, inp_vocab_size, embedding_size, enc_units, input_length):
        super().__init__()
        self.enc_units = enc_units
        self.embedding = Embedding(input_dim=inp_vocab_size, output_dim=300, input_length=input_length,
                                   mask_zero=True, name="embedding_layer_encoder")
        self.lstmcell = tf.keras.layers.LSTMCell(enc_units)
        self.enc = RNN(self.lstmcell, return_sequences=True, return_state=True)

    def call(self, input_sequence, states):
        embedding_enc = self.embedding(input_sequence)
        enc_output, enc_state_h, enc_state_c = self.enc(embedding_enc, initial_state=states)
        return enc_output, enc_state_h, enc_state_c

    def initialize_states(self, batch_size):
        ini_hidden_state = tf.zeros([batch_size, self.enc_units])
        ini_cell_state = tf.zeros([batch_size, self.enc_units])
        return [ini_hidden_state, ini_cell_state]


class Attention(tf.keras.layers.Layer):
    '''
      Class the calculates score based on the scoring_function using Bahdanu attention mechanism.
    '''

    def __init__(self, att_units):
        super().__init__()
        self.softmax = Softmax(axis=1)

    def call(self, decoder_hidden_state, encoder_output):
        attention_weight = tf.matmul(encoder_output, tf.expand_dims(decoder_hidden_state, axis=2))
        context = tf.matmul(tf.transpose(encoder_output, perm=[0, 2, 1]), attention_weight)
        context = tf.squeeze(context, axis=2)
        output = self.softmax(attention_weight)
        return context, output


class OneStepDecoder(tf.keras.Model):
    def __init__(self, tar_vocab_size, embedding_dim, input_length, dec_units, att_units):
        super().__init__()
        self.tar_vocab_size = tar_vocab_size
        self.dec_units = dec_units
        self.att_units = att_units
        self.embedding = Embedding(input_dim=tar_vocab_size, output_dim=300, input_length=input_length,
                                   mask_zero=True, name="embedding_layer_decoder")
        self.lstmcell = tf.keras.layers.LSTMCell(dec_units)
        self.decoder_lstm = RNN(self.lstmcell, return_sequences=True, return_state=True)
        self.dense = Dense(tar_vocab_size)
        self.attention = Attention(self.att_units)

    def call(self, input_to_decoder, encoder_output, state_h, state_c):
        embedding_layer = self.embedding(input_to_decoder)
        embedding_layer = tf.squeeze(embedding_layer, axis=1)
        context_vector, attention_weights = self.attention(state_h, encoder_output)
        context_vector_for_concat = tf.concat([context_vector, embedding_layer], 1)
        context_vector_for_concat = tf.expand_dims(context_vector_for_concat, 1)
        dec_output, dec_state_h, dec_state_c = self.decoder_lstm(context_vector_for_concat,
                                                                 initial_state=[state_h, state_c])
        output_after_dense_layer = self.dense(dec_output)
        output_after_dense_layer = tf.squeeze(output_after_dense_layer, axis=1)
        return output_after_dense_layer, dec_state_h, dec_state_c, attention_weights, context_vector


class Decoder(tf.keras.Model):
    def __init__(self, out_vocab_size, embedding_dim, input_length, dec_units, att_units):
        # Intialize necessary variables and create an object from the class onestepdecoder
        super().__init__()
        self.out_vocab_size = out_vocab_size
        self.embedding_dim = embedding_dim
        self.dec_units = dec_units
        self.att_units = att_units
        self.input_length = input_length
        self.onestepdecoder = OneStepDecoder(self.out_vocab_size, self.embedding_dim, self.input_length, self.dec_units,
                                             self.att_units)

    @tf.function
    def call(self, input_to_decoder, encoder_output, decoder_hidden_state, decoder_cell_state):
        all_outputs = tf.TensorArray(tf.float32, size=input_to_decoder.shape[1])
        for timestep in range(input_to_decoder.shape[1]):
            output, decoder_hidden_state, decoder_cell_state, attention_weights, context_vector = self.onestepdecoder(
                input_to_decoder[:, timestep:timestep + 1], encoder_output, decoder_hidden_state, decoder_cell_state)
            all_outputs = all_outputs.write(timestep, output)
        all_outputs = tf.transpose(all_outputs.stack(), [1, 0, 2])
        # print("all outpt shape is ",all_outputs.shape)
        return all_outputs


class encoder_decoder(tf.keras.Model):
    def __init__(self, inp_vocab_size, out_vocab_size, embedding_size, lstm_size, input_length, batch_size, att_units,
                 *args):
        super().__init__()  # https://stackoverflow.com/a/27134600/4084039
        self.encoder = Encoder(inp_vocab_size, embedding_size, lstm_size, input_length)
        self.decoder = Decoder(out_vocab_size, embedding_size, input_length, lstm_size, att_units)
        self.batch = batch_size

    def call(self, data):
        input, output = data[0], data[1]
        l = self.encoder.initialize_states(self.batch)
        encoder_output, encoder_final_state_h, encoder_final_state_c = self.encoder(input, l)
        decoder_output = self.decoder(output, encoder_output, encoder_final_state_h, encoder_final_state_c)
        return decoder_output


#https://www.tensorflow.org/tutorials/text/image_captioning#model
loss_object = tf.keras.losses.SparseCategoricalCrossentropy(from_logits=True, reduction='none')

def loss_function(real, pred):
    """ Custom loss function that will not consider the loss for padded zeros.
    in this loss function we are ignoring the loss
    for the padded zeros. i.e when the input is zero then we do not need to worry what the output is.
    This padded zeros are added from our end
    during preprocessing to make equal length for all the sentences.

    """
    mask = tf.math.logical_not(tf.math.equal(real, 0))
    loss_ = loss_object(real, pred)

    mask = tf.cast(mask, dtype=loss_.dtype)
    loss_ *= mask

    return tf.reduce_mean(loss_)


enc_vocab_size = len(tokenizer_enc.word_index) + 1
dec_vocab_size = len(tokenizer_dec.word_index) + 1
embedding_dim=300
input_length=12
lstm_size=192
batch_size=512
att_units = 192


train_trunc_idx = (train_enc_inp.shape[0]//batch_size)*batch_size
val_trunc_idx = (val_enc_inp.shape[0]//batch_size)*batch_size

train_enc_inp_truncated = train_enc_inp[:train_trunc_idx]
train_dec_inp_truncated = train_dec_inp[:train_trunc_idx]
train_dec_out_truncated = train_dec_out[:train_trunc_idx]

val_enc_inp_truncated = val_enc_inp[:val_trunc_idx]
val_dec_inp_truncated = val_dec_inp[:val_trunc_idx]
val_dec_out_truncated = val_dec_out[:val_trunc_idx]

model = encoder_decoder(enc_vocab_size,dec_vocab_size,embedding_dim,lstm_size,input_length,batch_size,att_units)

IST = pytz.timezone('Europe/Moscow')
log_dir=f'/content/drive/MyDrive/Self Case studies/CS02 Grammar Error Corrector/Models/03 enc_dec_with_attention/tb_logs/{datetime.now(IST).strftime("%Y%m%d%H%M%S")}'
tensorboard_callback = TensorBoard(log_dir=log_dir,histogram_freq=1,write_graph=True)
earlystop = EarlyStopping(monitor='val_loss', patience=10, verbose=1,mode='min')
reducelr = ReduceLROnPlateau(monitor='val_loss', patience=1, verbose=1, factor=0.9,mode='min')
check_point = ModelCheckpoint('/content/drive/MyDrive/Self Case studies/CS02 Grammar Error Corrector/Models/03 enc_dec_with_attention/03 enc_dec_with_attention', monitor='val_loss',
                              save_best_only=True, save_weights_only=True,mode='min', verbose=0)

model.compile(optimizer=Nadam(learning_rate=0.001),loss=loss_function)

model_history = model.fit(x=[train_enc_inp_truncated,train_dec_inp_truncated],y=train_dec_out_truncated,
                    validation_data=([val_enc_inp_truncated,val_dec_inp_truncated],val_dec_out_truncated),
                    epochs=20, batch_size=batch_size,callbacks=[tensorboard_callback,earlystop,reducelr,check_point])

model.summary()

